#!/usr/bin/python

import socket
import random

urls = ["https://www.instagram.com/", "https://chat.openai.com//", "http://www.facebook.com/",
        "https://www.twitch.tv/", "http://www.youtube.com/", "http://www.twitter.com/"]

myPort = 9999
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind(('', myPort))

mySocket.listen(5)

try:
    while True:
        random_url = random.choice(urls)
        print(f"Waiting for connections on port {myPort}...")
        (recvSocket, address) = mySocket.accept()
        print("HTTP request received:")
        print(recvSocket.recv(2048))
        redirect = "HTTP/1.1 302 Moved Temporarily\r\n" \
                   + "Location: " + str(random_url) + "\r\n" \
                   + "\r\n"
        recvSocket.send(redirect.encode('utf-8'))
        recvSocket.close()

except KeyboardInterrupt:
    print("Closing binded socket\n")
    mySocket.close()
